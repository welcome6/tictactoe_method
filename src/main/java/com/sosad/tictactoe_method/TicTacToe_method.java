/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sosad.tictactoe_method;

import java.util.Scanner;

/**
 *
 * @author Bunny0_
 */
public class TicTacToe_method {

    static boolean isFinish = false;
    static Scanner kb = new Scanner(System.in);

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char player = 'X';

    static int row, col;

    public static void main(String[] args) {
        do {
            showWelcome();
            showTable();
            showTurn();
            input();
            if (checkWin() != false) {
                break;
            }
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println("  1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println(" ");
        }

    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            // put_table
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("input again ");

        }
    }

    static boolean checkWin() {
        if (checkHorizontal() || checkVertical() || checkTopleft() 
                || checkTopright() == true) {
            return true;
        } else {
            return false;
        }

    }

    static boolean checkHorizontal() {
        for (int i = 0; i < table.length; i++) {
            //checkhorizontal
            if (table[i][0] == table[i][1] && table[i][0] == table[i][2] 
                    && table[i][0] != '-') {
                return true;
            }
        }
        return false;
    }

    static boolean checkVertical() {
        for (int i = 0; i < table.length; i++) {
            //checkvertical
            if (table[0][i] == table[1][i] && table[0][i] == table[2][i] 
                    && table[0][i] != '-') {
                return true;
            }
        }
        return false;
    }

    static boolean checkTopleft() {
        for (int i = 0; i < table.length; i++) {
            //check_top left>bottom right
            if (table[0][0] == table[1][1] && table[0][0] == table[2][2] 
                    && table[0][1] != '-') {
                return true;

            }
        }
        return false;
    }

    static boolean checkTopright() {
        for (int i = 0; i < table.length; i++) {
            //check_top right>bottom left
            if (table[2][0] == table[1][1] && table[2][0] == table[0][2] 
                    && table[2][0] != '-') {
                return true;
            }
        }
        return false;
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        System.out.println("Player " + player + " win ...");
    }

    static void showBye() {
        System.out.println("Bye bye ....");
    }
}
